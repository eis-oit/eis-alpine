
Simple image that runs Alpine Linux 3.12 

The following packages are pre-installed:
- bash
- vim

Default command is bash

Examples:
* Build this image:
`docker build -t eis/alpine .`

* Run image:
`docker run --rm -ti eis/alpine`

* Pull this image from the EIS Container Registry
`docker pull gitlab-registry.oit.duke.edu/eis-oit/images/eis-alpine:3.12`

* Run from container registry:
`docker run --rm -it gitlab-registry.oit.duke.edu/eis-oit/images/eis-alpine:3.12`

* Use this as a starting point for another project in your Dockerfile:
`FROM gitlab-registry.oit.duke.edu/eis-oit/images/eis-alpine:3.12`
