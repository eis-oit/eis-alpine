FROM alpine:3.12
LABEL maintainer="jeremy.hopkins@duke.edu"

RUN apk update \
    && \
    apk add --no-cache \
        bash \
        vim

CMD ["/bin/bash"]
